﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace YLScsDrawing.Drawing3d
{
    public class OriginTransform
    {
        // Store an alternative coordinate system, rotate it with the cube and retrive
        // the rotations needed to transform the system into intial position
        
        #region Approach 1

        private Quaternion rot_x, rot_y, rot_z;

        private Vector3d r0_x = new Vector3d(1, 0, 0),
                         r0_y = new Vector3d(0, 1, 0),
                         r0_z = new Vector3d(0, 0, 1);

        public OriginTransform()
        {
        }

        public OriginTransform(Quaternion x, Quaternion y, Quaternion z)
        {
            rot_x = Quaternion.getRotationQuaternion(x.V, r0_x);
            y.Rotate(rot_x);
            z.Rotate(rot_x);

            rot_y = Quaternion.getRotationQuaternion(y.V, r0_y);
            z.Rotate(rot_y);

            rot_z = Quaternion.getRotationQuaternion(z.V, r0_z);
        }

        public Quaternion getTranslationToOrigin()
        {
            return rot_x * rot_y * rot_z;
        }

        public Quaternion getTranslationFromOrigin()
        {
            return getTranslationToOrigin().getInverse();
            //return rot_z.getConjugate() * rot_y.getConjugate() * rot_x.getConjugate();
        }

        #endregion

        // Store a quaternion being a composition of all previous rotations,
        // which allows obtaining a reverse rotation to initial position

        #region Approach 2

        static public Quaternion rotationComposition;

        static public void addRotation(Quaternion q)
        {
            if (rotationComposition.Magnitude == 0)
                rotationComposition = q;
            else
                rotationComposition *= q;

            rotationComposition.Normalise();
        }

        static public void resetComposition()
        {
            rotationComposition = new Quaternion(0, 0, 0, 0);
        }

        #endregion
    }
}
