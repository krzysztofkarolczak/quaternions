using System;
using System.Collections.Generic;
using System.Drawing;

namespace YLScsDrawing.Drawing3d
{
    public class Cuboid : Shape3d
    {
        bool drawingLine = true, fillingFace = true;
        public bool DrawingLine
        {
            set { drawingLine = value; }
            get { return drawingLine; }
        }

        public bool FillingFace
        {
            set { fillingFace = value; }
            get { return fillingFace; }
        }

        Color[] faceColor = new Color[18] { Color.Red, Color.DarkGreen, Color.Yellow, Color.Orange, Color.Blue, Color.Purple,
                                            Color.Black, Color.Black, Color.Black, Color.Black, Color.Black, Color.Black, 
                                            Color.OrangeRed, Color.OrangeRed, Color.OrangeRed, Color.OrangeRed, Color.OrangeRed, Color.OrangeRed};
        public Color[] FaceColorArray
        {
            set
            {
                int n = Math.Min(value.Length, faceColor.Length);
                for (int i = 0; i < n; i++)
                    faceColor[i] = value[i];
            }
            get { return faceColor; }
        }

        public Cuboid(double a, double b, double c)
        {
            double base_b = 19 * b / 20;

            center = new Point3d(a / 2, b, c / 2);
            pts[0] = new Point3d(0, base_b, 0);
            pts[1] = new Point3d(a, base_b, 0);
            pts[2] = new Point3d(a, b, 0);
            pts[3] = new Point3d(0, b, 0);
            pts[4] = new Point3d(0, base_b, c);
            pts[5] = new Point3d(a, base_b, c);
            pts[6] = new Point3d(a, b, c);
            pts[7] = new Point3d(0, b, c);

            double start_a = 10 * a / 20;
            double end_a = 19 * a / 20;
            double start_b = 10 * b / 20;
            double end_b = 19 * b / 20;
            double start_c = 9 * c / 20;
            double end_c = 18 * c / 20;

            pts[8] = new Point3d(start_a, start_b, start_c);
            pts[9] = new Point3d(end_a, start_b, start_c);
            pts[10] = new Point3d(end_a, end_b, start_c);
            pts[11] = new Point3d(start_a, end_b, start_c);
            pts[12] = new Point3d(start_a, start_b, end_c);
            pts[13] = new Point3d(end_a, start_b, end_c);
            pts[14] = new Point3d(end_a, end_b, end_c);
            pts[15] = new Point3d(start_a, end_b, end_c);

            start_a = 13 * a / 20;
            end_a = 19 * a / 20;
            start_b = 17 * b / 20;
            end_b = 19 * b / 20;
            start_c = 1 * c / 20;
            end_c = 7 * c / 20;

            pts[16] = new Point3d(start_a, start_b, start_c);
            pts[17] = new Point3d(end_a, start_b, start_c);
            pts[18] = new Point3d(end_a, end_b, start_c);
            pts[19] = new Point3d(start_a, end_b, start_c);
            pts[20] = new Point3d(start_a, start_b, end_c);
            pts[21] = new Point3d(end_a, start_b, end_c);
            pts[22] = new Point3d(end_a, end_b, end_c);
            pts[23] = new Point3d(start_a, end_b, end_c);
        }

        
        public override void Draw(Graphics g, Camera cam)
        {
            PointF[] pts2d = cam.GetProjection(pts);

            PointF[][] face = new PointF[18][];
            face[0] = new PointF[] { pts2d[0], pts2d[1], pts2d[2], pts2d[3] };
            face[1] = new PointF[] { pts2d[5], pts2d[1], pts2d[0], pts2d[4] };
            face[2] = new PointF[] { pts2d[1], pts2d[5], pts2d[6], pts2d[2] };
            face[3] = new PointF[] { pts2d[2], pts2d[6], pts2d[7], pts2d[3] };
            face[4] = new PointF[] { pts2d[3], pts2d[7], pts2d[4], pts2d[0] };
            face[5] = new PointF[] { pts2d[4], pts2d[7], pts2d[6], pts2d[5] };

            face[6] = new PointF[] { pts2d[8], pts2d[9], pts2d[10], pts2d[11] };
            face[7] = new PointF[] { pts2d[13], pts2d[9], pts2d[8], pts2d[12] };
            face[8] = new PointF[] { pts2d[9], pts2d[13], pts2d[14], pts2d[10] };
            face[9] = new PointF[] { pts2d[10], pts2d[14], pts2d[15], pts2d[11] };
            face[10] = new PointF[] { pts2d[11], pts2d[15], pts2d[12], pts2d[8] };
            face[11] = new PointF[] { pts2d[12], pts2d[15], pts2d[14], pts2d[13] };

            face[12] = new PointF[] { pts2d[16], pts2d[17], pts2d[18], pts2d[19] };
            face[13] = new PointF[] { pts2d[21], pts2d[17], pts2d[16], pts2d[20] };
            face[14] = new PointF[] { pts2d[17], pts2d[21], pts2d[22], pts2d[18] };
            face[15] = new PointF[] { pts2d[18], pts2d[22], pts2d[23], pts2d[19] };
            face[16] = new PointF[] { pts2d[19], pts2d[23], pts2d[20], pts2d[16] };
            face[17] = new PointF[] { pts2d[20], pts2d[23], pts2d[22], pts2d[21] };

            for (int i = 0; i < face.Length; i++)
            {
                bool isout = false;
                for (int j = 0; j < 4; j++)
                {
                    if (face[i][j] == new PointF(float.MaxValue, float.MaxValue))
                    {
                        isout = true;
                        continue;
                    }
                }
                if (!isout)
                {
                    if (drawingLine)
                    {
                        if(i >= 12)
                            g.DrawPolygon(new Pen(Color.OrangeRed), face[i]);
                        else
                            g.DrawPolygon(new Pen(lineColor), face[i]);
                    }

                    if (YLScsDrawing.Geometry.Vector.IsClockwise(face[i][0], face[i][1], face[i][2])) // the face can be seen by camera
                    {
                        if (fillingFace) g.FillPolygon(new SolidBrush(faceColor[i]), face[i]);
                    }
                }
            }
        }
    }
}