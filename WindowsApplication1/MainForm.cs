using System;
using System.Linq;
using System.Data;
using System.Text;
using System.Drawing;
using System.IO.Ports;
using System.Windows.Forms;
using System.ComponentModel;
using System.Collections.Generic;

using System.Threading;
using System.IO;
using System.Diagnostics;
using ADIS;
using WindowsApplication1.Properties;
using YLScsDrawing.Drawing3d;

namespace WindowsApplication1
{
    public enum LogMsgType { Incoming, Outgoing, Normal, Warning, Error };


    public partial class MainForm : Form
    {
        #region Local Variables

        private ADISData adis_data_cur = new ADISData(), adis_data_old = new ADISData();

        //orientation
        int cameraX = 0, cameraY = 0, cameraZ = 0, cubeX = 0, cubeY = 0, cubeZ = 0;

        Cuboid cub = new Cuboid(150, 150, 150);
        Camera cam = new Camera();

        //Quaternions storing the "universe" coordinate system
        Quaternion r_x = new Quaternion(0, 1, 0, 0);
        Quaternion r_y = new Quaternion(0, 0, 1, 0);
        Quaternion r_z = new Quaternion(0, 0, 0, 1);

        // The main control for communicating through the RS-232 port
        private SerialPort comport = new SerialPort();

        // Various colors for logging info
        private Color[] LogMsgTypeColor = { Color.Blue, Color.Green, Color.Black, Color.Orange, Color.Red };
        private Settings settings = Settings.Default;

        #endregion

        #region FormInitialization

        public MainForm()
        {
            // Load user settings
            settings.Reload();

            // Build the form
            InitializeComponent();

            // Restore the users settings
            InitializeControlValues();

            // Enable/disable controls based on the current state
            EnableControls();

            // When data is recieved through the port, call this method
            comport.DataReceived += new SerialDataReceivedEventHandler(port_DataReceived);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            cub.Center = new Point3d(350, 240, 0);
            cam.Location = new Point3d(350, 240, -500);
            Invalidate();

            OriginTransform.resetComposition();
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            cub.Draw(e.Graphics, cam);
        }

        #endregion

        #region SimpleButtons

        private void button12_Click(object sender, EventArgs e)
        {
            cubeX += 5;
            Quaternion q = new Quaternion();
            q.FromAxisAngle(new Vector3d(1, 0, 0), 5 * Math.PI / 180.0);
            cub.RotateAt(cub.Center, q);
            Invalidate();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            cubeX -= 5;
            Quaternion q = new Quaternion();
            q.FromAxisAngle(new Vector3d(1, 0, 0), -5 * Math.PI / 180.0);
            cub.RotateAt(cub.Center, q);
            Invalidate();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            cubeY += 5;
            Quaternion q = new Quaternion();
            q.FromAxisAngle(new Vector3d(0, 1, 0), 5 * Math.PI / 180.0);
            cub.RotateAt(cub.Center, q);
            Invalidate();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            cubeY -= 5;
            Quaternion q = new Quaternion();
            q.FromAxisAngle(new Vector3d(0, 1, 0), -5 * Math.PI / 180.0);
            cub.RotateAt(cub.Center, q);
            Invalidate();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            cubeZ += 5;
            Quaternion q = new Quaternion();
            q.FromAxisAngle(new Vector3d(0, 0, 1), 5 * Math.PI / 180.0);
            cub.RotateAt(cub.Center, q);
            Invalidate();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            cubeZ -= 5;
            Quaternion q = new Quaternion();
            q.FromAxisAngle(new Vector3d(0, 0, 1), -5 * Math.PI / 180.0);
            cub.RotateAt(cub.Center, q);
            Invalidate();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            cam.MoveLeft(10);
            Invalidate();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            cam.MoveRight(10);
            Invalidate();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            cam.MoveUp(10);
            Invalidate();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            cam.MoveDown(10);
            Invalidate();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            cam.MoveIn(10);
            Invalidate();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            cam.MoveOut(10);
            Invalidate();
        }

        private void button18_Click(object sender, EventArgs e)
        {
            cameraY -= 1;
            cam.TurnLeft(1);
            Invalidate();
        }

        private void button17_Click(object sender, EventArgs e)
        {
            cameraY += 1;
            cam.TurnRight(1);
            Invalidate();
        }

        private void button16_Click(object sender, EventArgs e)
        {
            cameraX -= 1;
            cam.TurnUp(1);
            Invalidate();
        }

        private void button15_Click(object sender, EventArgs e)
        {
            cameraX += 1;
            cam.TurnDown(1);
            Invalidate();
        }

        private void button26_Click(object sender, EventArgs e)
        {
            cameraZ += 5;
            cam.Roll(-5);
            Invalidate();
        }

        private void button25_Click(object sender, EventArgs e)
        {
            cameraZ -= 5;
            cam.Roll(5);
            Invalidate();
        }

        private void button24_Click(object sender, EventArgs e)
        {
            cub.Center = new Point3d(cub.Center.X - 5, cub.Center.Y, cub.Center.Z);
            Invalidate();
        }

        private void button23_Click(object sender, EventArgs e)
        {
            cub.Center = new Point3d(cub.Center.X + 5, cub.Center.Y, cub.Center.Z);
            Invalidate();
        }

        private void button22_Click(object sender, EventArgs e)
        {
            cub.Center = new Point3d(cub.Center.X, cub.Center.Y - 5, cub.Center.Z);
            Invalidate();
        }

        private void button21_Click(object sender, EventArgs e)
        {
            cub.Center = new Point3d(cub.Center.X, cub.Center.Y + 5, cub.Center.Z);
            Invalidate();
        }

        private void button20_Click(object sender, EventArgs e)
        {
            cub.Center = new Point3d(cub.Center.X, cub.Center.Y, cub.Center.Z + 5);
            Invalidate();
        }

        private void button19_Click(object sender, EventArgs e)
        {
            cub.Center = new Point3d(cub.Center.X, cub.Center.Y, cub.Center.Z - 5);
            Invalidate();
        }

        #endregion

        private void resetButton_Click(object sender, EventArgs e)
        {
            cub = new Cuboid(150, 150, 150);
            cam = new Camera();
            cub.Center = new Point3d(350, 240, 0);
            cam.Location = new Point3d(350, 240, -500);
            Invalidate();

            cameraX = 0; cameraY = 0; cameraZ = 0; cubeX = 0; cubeY = 0; cubeZ = 0;

            r_x = new Quaternion(0, 1, 0, 0);
            r_y = new Quaternion(0, 0, 1, 0);
            r_z = new Quaternion(0, 0, 0, 1);
            OriginTransform.resetComposition();
        }

        private void btnShowMore_Click(object sender, EventArgs e)
        {
            if (MainForm.ActiveForm.Height > 500)
            {
                MainForm.ActiveForm.Height = 455;
                btnShowMore.Text = "Hide Additional Options";
            }
            else
            {
                MainForm.ActiveForm.Height = 640;
                btnShowMore.Text = "Show Additional Options";
            }
        }

        #region SerialPort

        #region Local Methods

        /// <summary> Save the user's settings. </summary>
        private void SaveSettings()
        {
            settings.BaudRate = int.Parse(cmbBaudRate.Text);
            settings.DataBits = int.Parse(cmbDataBits.Text);
            settings.Parity = (Parity)Enum.Parse(typeof(Parity), cmbParity.Text);
            settings.StopBits = (StopBits)Enum.Parse(typeof(StopBits), cmbStopBits.Text);
            settings.PortName = cmbPortName.Text;

            settings.Save();
        }

        /// <summary> Populate the form's controls with default settings. </summary>
        private void InitializeControlValues()
        {
            cmbParity.Items.Clear(); cmbParity.Items.AddRange(Enum.GetNames(typeof(Parity)));
            cmbStopBits.Items.Clear(); cmbStopBits.Items.AddRange(Enum.GetNames(typeof(StopBits)));

            cmbParity.Text = settings.Parity.ToString();
            cmbStopBits.Text = settings.StopBits.ToString();
            cmbDataBits.Text = settings.DataBits.ToString();
            cmbParity.Text = settings.Parity.ToString();
            cmbBaudRate.Text = settings.BaudRate.ToString();

            RefreshComPortList();

            // If it is still avalible, select the last com port used
            if (cmbPortName.Items.Contains(settings.PortName)) cmbPortName.Text = settings.PortName;
            else if (cmbPortName.Items.Count > 0) cmbPortName.SelectedIndex = cmbPortName.Items.Count - 1;
            else
            {
                MessageBox.Show(this, "There are no COM Ports detected on this computer.\nPlease install a COM Port and restart this app.", "No COM Ports Installed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }

        /// <summary> Enable/disable controls based on the app's current state. </summary>
        private void EnableControls()
        {
            // Enable/disable controls based on whether the port is open or not
            gbPortSettings.Enabled = !comport.IsOpen;

            if (comport.IsOpen) btnOpenPort.Text = "&Close Port";
            else btnOpenPort.Text = "&Open Port";
        }

        /// <summary> Log data to the terminal window. </summary>
        /// <param name="msgtype"> The type of message to be written. </param>
        /// <param name="msg"> The string containing the message to be shown. </param>
        private void Log(LogMsgType msgtype, string msg)
        {
            rtfTerminal.Invoke(new EventHandler(delegate
            {
                rtfTerminal.SelectedText = string.Empty;
                rtfTerminal.SelectionFont = new Font(rtfTerminal.SelectionFont, FontStyle.Bold);
                rtfTerminal.SelectionColor = LogMsgTypeColor[(int)msgtype];
                rtfTerminal.AppendText(msg);
                rtfTerminal.ScrollToCaret();
            }));
        }

        #endregion

        #region Event Handlers

        private void frmTerminal_Shown(object sender, EventArgs e)
        {
            Log(LogMsgType.Normal, String.Format("Application Started at {0}\n", DateTime.Now));
        }
        private void frmTerminal_FormClosing(object sender, FormClosingEventArgs e)
        {
            // The form is closing, save the user's preferences
            SaveSettings();
        }

        private void cmbBaudRate_Validating(object sender, CancelEventArgs e)
        { int x; e.Cancel = !int.TryParse(cmbBaudRate.Text, out x); }

        private void cmbDataBits_Validating(object sender, CancelEventArgs e)
        { int x; e.Cancel = !int.TryParse(cmbDataBits.Text, out x); }

        private void btnOpenPort_Click(object sender, EventArgs e)
        {
            bool error = false;

            // If the port is open, close it.
            if (comport.IsOpen) comport.Close();
            else
            {
                // Set the port's settings
                comport.BaudRate = int.Parse(cmbBaudRate.Text);
                comport.DataBits = int.Parse(cmbDataBits.Text);
                comport.StopBits = (StopBits)Enum.Parse(typeof(StopBits), cmbStopBits.Text);
                comport.Parity = (Parity)Enum.Parse(typeof(Parity), cmbParity.Text);
                comport.PortName = cmbPortName.Text;

                try
                {
                    // Open the port
                    comport.Open();
                }
                catch (UnauthorizedAccessException) { error = true; }
                catch (IOException) { error = true; }
                catch (ArgumentException) { error = true; }
                catch (Exception) { };

                if (error) MessageBox.Show(this, "Could not open the COM port.  Most likely it is already in use, has been removed, or is unavailable.", "COM Port Unavalible", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }

            // Change the state of the form's controls
            EnableControls();

        }

        #endregion

        private void port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            // If the com port has been closed, do nothing
            if (!comport.IsOpen) return;

            // This method will be called when there is data waiting in the port's buffer
            // Read all the data waiting in the buffer
            try
            {
                string data = comport.ReadLine();

                ADISData adis_data = ADISHelper.ADIS_GetData(data);

                if (adis_data != null)
                {
                    adis_data_old = adis_data_cur;
                    adis_data_cur = adis_data;
                    int sample_diff = adis_data_cur.sampleNum - adis_data_old.sampleNum;

                    if(sample_diff != 1 && adis_data_old.sampleNum != 0)
                        Log(LogMsgType.Warning, "Lost " + (sample_diff-1).ToString() + " sample(s)");

                    if (adis_data_cur.isAntiDriftLocked() == false)
                        PerformRotation(adis_data_cur, adis_data_old);
        
                    // Display the text to the user in the terminal
                    // Log(LogMsgType.Incoming, data);
                     Log(LogMsgType.Normal, adis_data_cur.ToString());
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                Log(LogMsgType.Error, ex.Message);
                return;
            }

        }

        private void PerformRotation(ADISData adis_data_cur, ADISData adis_data_old)
        {

            Quaternion q = new Quaternion();
            q.FromAngularVelocity(adis_data_old.gyr, 0.0156);

            //TimeSpan timeElapse = adis_data_cur.aTime - adis_data_old.aTime;
            //Debug.WriteLine(timeElapse.TotalSeconds.ToString());
            //q.FromAngularVelocity(adis_data_old.gyr, timeElapse.TotalSeconds); //0.0156

            #region Approach 1 - storing coordinate system and retrieving inverse rotation

            //We transform the cube to initial position rotate by q and transform back

            OriginTransform transform = new OriginTransform(r_x, r_y, r_z);
            Quaternion rotation_with_translation = transform.getTranslationFromOrigin() * q * transform.getTranslationToOrigin();
            cub.RotateAt(cub.Center, rotation_with_translation);

            r_x.Rotate(rotation_with_translation);
            r_y.Rotate(rotation_with_translation);
            r_z.Rotate(rotation_with_translation);

            #endregion

            #region Approach 2 - quaternion composition of all rotations

            //if (OriginTransform.rotationComposition.Magnitude != 0)
            //    q.Rotate(OriginTransform.rotationComposition);

            //cub.RotateAt(cub.Center, q);
            //OriginTransform.addRotation(q);

            #endregion

            #region Approach 3 - without translation

            // Without translation
            //cub.RotateAt(cub.Center, q); 

            #endregion

            UpdateADISDisplay(adis_data_cur, adis_data_old);

            Invalidate();
        }

        private void UpdateADISDisplay(ADISData adis_data, ADISData adis_data_old)
        {

            groupBox3.Invoke(new EventHandler(delegate{
                label11.Text = adis_data.gyr.X.ToString("0.0");
                label12.Text = adis_data.gyr.Y.ToString("0.0");
                label13.Text = adis_data.gyr.Z.ToString("0.0");

                label8.Text = adis_data.acc.X.ToString("0.0");
                label9.Text = adis_data.acc.Y.ToString("0.0");
                label10.Text = adis_data.acc.Z.ToString("0.0");
            }));
        }

        #region SerialPortControls

        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearTerminal();
        }

        private void ClearTerminal()
        {
            rtfTerminal.Clear();
        }

        private void tmrCheckComPorts_Tick(object sender, EventArgs e)
        {
            // checks to see if COM ports have been added or removed
            // since it is quite common now with USB-to-Serial adapters
            RefreshComPortList();
        }

        private void RefreshComPortList()
        {
            // Determain if the list of com port names has changed since last checked
            string selected = RefreshComPortList(cmbPortName.Items.Cast<string>(), cmbPortName.SelectedItem as string, comport.IsOpen);

            // If there was an update, then update the control showing the user the list of port names
            if (!String.IsNullOrEmpty(selected))
            {
                cmbPortName.Items.Clear();
                cmbPortName.Items.AddRange(OrderedPortNames());
                cmbPortName.SelectedItem = selected;
            }
        }

        private string[] OrderedPortNames()
        {
            // Just a placeholder for a successful parsing of a string to an integer
            int num;

            // Order the serial port names in numberic order (if possible)
            return SerialPort.GetPortNames().OrderBy(a => a.Length > 3 && int.TryParse(a.Substring(3), out num) ? num : 0).ToArray();
        }

        private string RefreshComPortList(IEnumerable<string> PreviousPortNames, string CurrentSelection, bool PortOpen)
        {
            // Create a new return report to populate
            string selected = null;

            // Retrieve the list of ports currently mounted by the operating system (sorted by name)
            string[] ports = SerialPort.GetPortNames();

            // First determain if there was a change (any additions or removals)
            bool updated = PreviousPortNames.Except(ports).Count() > 0 || ports.Except(PreviousPortNames).Count() > 0;

            // If there was a change, then select an appropriate default port
            if (updated)
            {
                // Use the correctly ordered set of port names
                ports = OrderedPortNames();

                // Find newest port if one or more were added
                string newest = SerialPort.GetPortNames().Except(PreviousPortNames).OrderBy(a => a).LastOrDefault();

                // If the port was already open... (see logic notes and reasoning in Notes.txt)
                if (PortOpen)
                {
                    if (ports.Contains(CurrentSelection)) selected = CurrentSelection;
                    else if (!String.IsNullOrEmpty(newest)) selected = newest;
                    else selected = ports.LastOrDefault();
                }
                else
                {
                    if (!String.IsNullOrEmpty(newest)) selected = newest;
                    else if (ports.Contains(CurrentSelection)) selected = CurrentSelection;
                    else selected = ports.LastOrDefault();
                }
            }

            // If there was a change to the port list, return the recommended default selection
            return selected;
        }

        #endregion

        #endregion
    }
}