﻿using System;
using System.Collections.Generic;
using System.Text;
using YLScsDrawing.Drawing3d;

namespace ADIS
{
    class ADISData
    {
        //---------------------------------------------------------------------------
        // converts to physical values
        // GYR_LSB = 0.07326
        // ACC_LSB = 2.5mg

        const double GYR_LSB = 0.07326 / 60;
        const double ACC_LSB = 0.0025;

        // How many steady state samples are needed for drift lock
        const int samples_to_lock = 50;

        static bool drift_lock = false;
        static int samples_for_lock = 0;

        private Vector3d _gyr;
        private Vector3d _acc;
        public DateTime aTime;

        public int sampleNum;

        public ADISData()
        {
        }

        public ADISData(int gX, int gY, int gZ, int aX, int aY, int aZ, int num)
        {
            gyr = new Vector3d(gX, -gZ, gY); // x, y, z swapped to adapt to your display sceen 
            acc = new Vector3d(aX, aY, aZ); 
            aTime = DateTime.Now;
            sampleNum = num;
        }

        public bool isAntiDriftLocked()
        {
            if (acc.Magnitude > 0.965 && acc.Magnitude < 1.035)
            {
                samples_for_lock++;
                drift_lock = (samples_for_lock > samples_to_lock) ? true : false;
            }
            else
            {
                samples_for_lock = 0;
                drift_lock = false;
            }

            return drift_lock;
        }

        public Vector3d gyr
        {
            get
            {
                return _gyr;
            }
            set
            {
                value.X *= GYR_LSB;
                value.Y *= GYR_LSB;
                value.Z *= GYR_LSB;

                _gyr = value;
            }
        }

        public Vector3d acc
        {
            get
            {
                return _acc;
            }
            set
            {
                value.X *= ACC_LSB;
                value.Y *= ACC_LSB;
                value.Z *= ACC_LSB;

                _acc = value;
            }
        }

        public override string ToString()
        {
            return "GYR | x: " + toStringHelper(gyr.X) + ", y: " + toStringHelper(gyr.Y) + ", z: " + toStringHelper(gyr.Z) + "\n";
                   //+ "ACC | x: " + acc.X.ToString() + ", y: " + acc.Y.ToString() + ", z: " + acc.Z.ToString();
        }

        public string toStringHelper(double a)
        {
            if (a >= 0)
                return a.ToString(" 0.000");
            else
                return a.ToString("0.000");
        }



    }
}
