﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace ADIS
{
    static class ADISHelper
    {
        // Code from Przemysław Barański PhD, addapted from C++ to C#

        //---------------------------------------------------------------------------
        // converts measurements written in a string into data of int type
        // startPos indicates where the first measurement begins ("#" symbol)
        //         1         2         3         4         5
        //123456789012345678901234567890123456789012345678901234567890
        //0000004479 #80#0B #BF#F2 #80#55 #80#16 #BF#FD #BE#70
        //           gyrX   gyrY   gyrZ   accX   accY   accZ
        public static ADISData ADIS_GetData(string line)
        {
            int gyrX, gyrY, gyrZ, accX, accY, accZ, num;

            if (line.Length < 52)
                return null; //throw new Exception("Data too short - could be GPS. Ignoring line.");

            if (line[11] != '#')
                throw new Exception("Data corupted. Ignoring line.");

            try {

                num = int.Parse(line.Substring(0, 10));

                gyrX = ADIS_GetSingleData(12, line);
                gyrY = ADIS_GetSingleData(19, line);
                gyrZ = ADIS_GetSingleData(26, line);

                accX = ADIS_GetSingleData(33, line);
                accY = ADIS_GetSingleData(40, line);
                accZ = ADIS_GetSingleData(47, line);

            } catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }

            return new ADISData(gyrX, gyrY, gyrZ, accX, accY, accZ, num);
        }
        //---------------------------------------------------------------------------
        static int ADIS_GetSingleData(int startPos, string line)
        {
            int singleData;
            string hexA, hexB;
            int n1,n2;

            if( startPos + 4 > line.Length )
                throw new Exception("Data ends unexpectedly.");

            hexA = line.Substring(startPos + 3, 2);
            n1 = Convert.ToInt32(hexA, 16);
            n1 &= 0xFF;

            hexB = line.Substring(startPos, 2);
            n2 = Convert.ToInt32(hexB, 16);
            n2 &= 0xFF;

            singleData = ( n1 & 0xFF ) | ( ( n2 << 8 ) & 0xFF00 );
            singleData &= 0x3FFF;

            if((singleData & (1 << 13 )) != 0)
                singleData = (int)((uint)singleData | 0xFFFFC000);

            return singleData;
        }




    }
}
